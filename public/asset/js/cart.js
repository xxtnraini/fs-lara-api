const cart = () => {
  var sum = 0;
  const total = document.querySelector(".card");
  fetch('api/getCart')
  .then(response => {
      if (response.status === 200) {
          return response.json();
      } else {
          throw new Error('Something went wrong on api server!');
      }
  })
  .then((data) => {
      let menu = "";
      data.forEach((pesan) => {
          menu += ` 
      <div class="container" style="margin-right:1px;">
        <div class="row" style="float: left;">
          <div class="col-sm m-2" >
            <div class="card m-3" style="width: 18rem;">
              <img src="${item.gambar}" class="card-img-top" alt="...">
              <div class="card-body bg-light">
                <h5 class="card-title">${item.judul}</h5>
                <p><b>Rp.${item.harga}</b></p><br>
                <p class="card-text">${item.deskripsi}</p>
                <i class="fas fa-star text-success"></i>
                <i class="fas fa-star text-success"></i>
                <i class="fas fa-star text-success"></i>
                <i class="fas fa-star text-success"></i><br>
                <a href="#" class="btn btn-primary" data-target="#menu1" data-toggle="modal">Detail</a>
                <a class="btn btn-danger">Beli Sekarang</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    `
          
      });
      const section = document.querySelector(".total");
      section.innerHTML = menu;
      sum = data.reduce((accumulator, curentvalue) => {
        return accumulator + parseInt(curentvalue.harga);
      }, 0);
    total.innerHTML = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  });
};
cart();

