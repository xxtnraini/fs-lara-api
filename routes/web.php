<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\menuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
}); 


Route::get('/home', function(){
    return view('home');
});

Route::get('/admin', function(){
    return view('admin');
});
Route::get('/menu', 'MenuController@home');
Route::post('/Menu/create', 'MenuController@create');
Route::get('/Menu/{id}/edit', 'MenuController@edit');
Route::post('/Menu/{id}/update', 'MenuController@update');
Route::get('/Menu/{id}/delete', 'MenuController@delete');
