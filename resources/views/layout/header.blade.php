
    <nav class="navbar navbar-expand-lg navbar-dark bg-warning">
      <div class="container">
        <img src="{{ asset('asset/LOGO.png') }}" width="90px" height="90px">
        <a class="navbar-brand font-weight-bold" href="#">Food Station</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>


 <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mr-4">
            <li class="nav-item active">
              <a class="nav-link" href="#">Beranda <span class="sr-only">(current)</span></a>
            </li>
          <div class="form-inline">
            <input class="input-keyword form-control mr-sm-2" type="search" placeholder="cari disini" aria-label="Search">
            <button class="button-keyword btn btn-outline-terang my-2 my-sm-0" type="submit">Cari</button>
          </div>
          <li class="nav-item px-6 d-flex">
       <a href="/admin">
          <img src="{{ asset('asset/admin.png') }}" width="50px" height="50px"alt="cart-icon" class="admin"/>
       </a>
      </li>
          <div class="icon mt-2">
            <h5>
              <i class="fas fa-cart-plus ml-3 mr-3" data-toggle="tooltip" title="Keranjang Belanja"></i>
              <i class="fas fa-envelope mr-3" data-toggle="tooltip" title="Pesanan"></i>
              <i class="fas fa-bell mr-3" data-toggle="tooltip" title="Notifikasi"></i>
            </h5>
          </div>
        </div>
      </div>
    </nav>



    <div class="row mt-5 no-gutters" style="flex-wrap: nowrap; margin-top: 0 !important;">
      <div class="col-md-2 bg-light">
        <ul class="list-group list-group-flush p-2 pt-4">
          <li class="list-group-item bg-warning"> <i class="fas fa-list"></i> MENU</li>
          <li class="list-group-item"> <i class="fas fa-angle-right"></i> <a style="color: black" href="#"> Set Menu</a></li>
          <li class="list-group-item"> <i class="fas fa-angle-right"></i><a  style="color: black" href="#"> Main Menu</a></li>
          <li class="list-group-item"> <i class="fas fa-angle-right"></i> <a style="color: black" href="#"> Fried Menu </a></li>
          <li class="list-group-item"> <i class="fas fa-angle-right"></i><a style="color: black" href="#"> Dessert</a></li>
          <li class="list-group-item"> <i class="fas fa-angle-right"></i><a style="color: black" href="#"> Side dish</a></li>
        </ul>
      </div>   
