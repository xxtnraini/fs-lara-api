<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ADMIN</title>
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
        .search input[type=search] {
            width: 1000px
        }
        .arrow-back{
            position: absolute;
        }
        @media (max-width: 1366px) and (max-height: 768px){
            .search {
                transform: scale(0.7);
            }
            .search input[type=search] {
                width: 700px;
            }
        }
    </style>
</head>
<body>





        <!--navbar-->
            <nav class="navbar navbar-light bg-danger" style="height: 90px;">
      <a class="navbar-brand" href="/admin">
        <img src="assets/img/back.png" id="home">
      </a>  
          <h5 class="nav-item active col font-weight-bold">DATA MENU</h5> 
       <div class="col mb-5">
         <button type="button" class="btn btn-danger btn-bg float-right text-light" data-toggle="modal" data-target="#exampleModal"><img src="assets/img/order/plus.png" id="plus"></button>
       </div>
  </nav>
      
<div class="container-admin" style="margin: 10px">
    @if(session('success'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Succes!</strong> {{session('success')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

        <table class="table table-hover table-striped">
            <thead>
                <tr class="bg-danger text-light">
                    <th>GAMBAR</th>
                    <th>NAME</th>
                    <th>Harga</th>
                    <th style="padding-left: 50px; padding-right: 50px; text-align: left;"></th>
                </tr>
            </thead>

            @foreach ($menu as $data)
            <tr>
                <td>{{$data->gambar}}</td>
                <td>{{$data->nama}}</td>               
                <td>{{$data->harga}}</td>
                <td>
                    <a href="/menu/{{$data->id}}/edit" class="btn btn-dark btn-sm text-light">EDIT</a>
                    <a href="/menu/{{$data->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Kamu Yakin Data Akan Dihapus?')">DELETE</a>
                </td>
            </tr>
            @endforeach

        </table>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-light"  style="background: pink;">
            <div class="modal-header" style="border-bottom: 2px solid white;">
                <h5 class="modal-title" id="exampleModalLabel">ADD MENU</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- Popup Forms --}}
                <form action="/menu/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Gambar</label>
                        <input style="border:none" name="gambar" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="GAMBAR" value="assets/">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Menu</label>
                        <input style="border:none" name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NAMA MENU">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Harga</label>
                        <input style="border:none" name="harga" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="HARGA">
                    </div>
                </div>
                <div class="modal-footer" style="border-top: 2px solid white;">
                    <button type="button" class="btn btn-danger text-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark text-light">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>


