<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="fontawesome-free/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">

    <title>Food Station</title>
    <style type="text/css">
      .card p {
          margin-top: -10px;
          margin-bottom: 1px;
        }
        .row .card:hover{
          box-shadow: 2px 2px 2px rgba(0,0,0,0.4);
          transform: scale(1.02);
        }
        .h4{
          font-color: red;
        }
        .kolom-pesan{
          width: 300px;
            height: 100px;
            margin: 0 auto;
            margin-bottom: 20px;
            padding: 20px;
            padding-bottom: 10px;
            display: flex;
            flex-direction: column;
            align-items: center;
            box-shadow: 0 0 10px 5px #ffba08;
            color: #000000;
        }
        .kolom-pesan h2{
          font-size: 25px;
          margin-bottom: 15px;
        }
        footer{
  background-color: orange;
  color: black;
  padding: 10px 200px;
}

footer > h3{
  margin: 0;
  font-size: 20px;
  font-weight: bold;
}

footer > .contact{
  display: inline-block;
}

footer > .contact > p{
  float: left;
  margin: 0;
  margin-right: 30px;
}

footer > .contact > .text-call{
  font-size: 18px;
  font-weight: bold;
}

footer > .kebijakan{
  margin-top: 10px;
  color:black;
  font-size: 14px;
}
.home{
  width: 25px;
  height: 25px;
}
    </style>
  </head>
  <body>
<!--navbar-->
    @include('layout.header')
    
     <div class="col-md-10">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="{{ asset('/asset/01.jpg') }}" class="d-block w-100" alt="..." height="537">
            </div>
            <div class="carousel-item">
              <img src="{{ asset('/asset/02.jpg') }}" class="d-block w-100" alt="..." height="537">
            </div>
            <div class="carousel-item">
              <img src="{{ asset('/asset/03.jpg') }}" class="d-block w-100" alt="..." height="537">
            </div>
            <div class="carousel-item">
              <img src="{{ asset('/asset/04.jpg') }}" class="d-block w-100" alt="..." height="537">
            </div>
            <div class="carousel-item">
              <img src="{{ asset('/asset/05.jpg') }}" class="d-block w-100" alt="..." height="537">
            </div>
          </div>

          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
</div>
</div>
</div>
        <h4 class="text-center font-weight-bold m-4 style= font-color: red;"> HOT  MENU</h4>
          <div class="kolom-pesan">
        <h2>jumlahMenu</h2>
        <h3></h3>
      </div>

      <div class="makanan-menu"> </div>
      <script type="text/javascript" src="script.js"></script>
<!--footer-->
    @include('layout.footer')

    
<!--optional javas-->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('asset/js/script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('asset/js/data.js') }}"></script>
  </body>
</html>

